<?php

/**
 * @file
 * Rules actions definitions for sending SMS messages
 */

/**
 * Implementation of hook_rules_action_info().
 */
function rules_send_sms_rules_action_info() {
  return array(
    'rules_send_sms_send' => array(
      'label' => t('Send an SMS'),
      'module' => 'SMS',
      'eval input' => array(
        'numbers',
        'message',
      ),
    ),
    'rules_send_sms_send_to_user' => array(
      'label' => t('Send an SMS to user(s)'),
      'module' => 'SMS',
      'eval input' => array(
        'uids',
        'message',
      ),
    ),
  );
}

function rules_send_sms_send_to_user($settings) {
  if (module_exists('sms_user')) {
    $message = $settings['message'];
    $uids_field = $settings['uids'];
    
    // Split up our recipient user ids
    $uids = array();
    foreach (explode(",", $uids_field) as $uid) {
      $uids[] = trim($uid);
    }

    foreach ($uids as $uid) {
      $user = user_load($uid);
      if ($user) {
        $name = $user->name;
        $sent = sms_user_send($uid, $message);
      }
      else {
        watchdog('rules', 'Attempt to send SMS to uid @uid failed.', array('@uid' => $uid), WATCHDOG_ERROR);
      }

      if (!$sent) {
        watchdog('rules', 'Attempt to send SMS to @name failed.', array('@name' => $name), WATCHDOG_ERROR);
      }
    }
  } 
  else {
    watchdog('rules', 'The module sms_user is needed for using this action.', WATCHDOG_ERROR);
  }  
}

function rules_send_sms_send($settings) {
  $message = $settings['message'];
  $numbers_field = $settings['numbers'];
  
  // Split up our recipient user ids
  $numbers = array();
  foreach (explode(",", $numbers_field) as $number) {
    $numbers[] = trim($number);
  }

  foreach ($numbers as $number) {
    $sent = sms_send($number, $message);
    if (!$sent) {
      watchdog('rules', 'Attempt to send SMS to @number failed.', array('@number' => $number), WATCHDOG_ERROR);
    }
  }
}