<?php

/**
 * @file
 * Rules forms for SMS sending actions
 */

function rules_send_sms_send_form($settings = array(), &$form) {

  $form['settings']['numbers'] = array(
    '#title' => t('Phone numbers'),
    '#type' => 'textfield',
    '#default_value' => $settings['numbers'],
    '#description' => t("Comma-separated list of phone numbers."),
  );
  $form['settings']['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#default_value' => $settings['message'],
    '#description' => t("The SMS message, up to 140 chars."),
  );
}

function rules_send_sms_send_to_user_form($settings = array(), &$form) {

  $form['settings']['uids'] = array(
    '#title' => t('User IDs'),
    '#type' => 'textfield',
    '#description' => t("Comma-separated list of user IDs."),
    '#default_value' => $settings['uids'],
  );
  $form['settings']['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#default_value' => $settings['message'],
    '#description' => t("The SMS message, up to 140 chars."),
  );
}

